﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryWindowsPhone.Model
{
    public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public System.DateTime ReleaseDate { get; set; }
        public System.DateTime AddDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ISBN { get; set; }
        public string BarCode { get; set; }
        public string Publisher { get; set; }
        public int Count { get; set; }

        public int AvailableCount { get; set; }

        public IList<Category> Categories { get; set; }


    }
}
