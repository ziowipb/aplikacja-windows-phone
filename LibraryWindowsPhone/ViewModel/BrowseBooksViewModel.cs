﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LibraryWindowsPhone.Helper;
using LibraryWindowsPhone.Model;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace LibraryWindowsPhone.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class BrowseBooksViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the BrowseBooksViewModel class.
        /// </summary>
        public BrowseBooksViewModel()
        {
            Books = (List<Book>)PhoneApplicationService.Current.State["books"];
            SearchBooksPageCommand = new RelayCommand(() => GoToSearchBooksPage());
            DetailsBookPageCommand = new RelayCommand<Book>(GoToDetailsBookPage);
            //WebClient webClient = new WebClient();
            //webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(DownloadStringCompleted);
            //webClient.DownloadStringAsync(new Uri("http://mswiecki-001-site1.myasp.net/api/getbooks?gatunek=&author=&isbn=&title=", UriKind.Absolute));
            
        }

        

        
        //private IList<Book> _books;
        public ICommand DetailsBookPageCommand { get; set; }

        public void GoToDetailsBookPage(Book book)
        {
            PhoneApplicationService.Current.State["details"] = book;
            var MyFrame = Application.Current.RootVisual as Frame;
            MyFrame.Navigate(new Uri("/View/DetailsBookView.xaml", UriKind.Relative));
        }

        private IList<Book> _books = new List<Book>();
        public IList<Book> Books 
        { 
            get
            {
                return _books;
            }
            set
            {
                _books = value;
                
                Horror = ReturnBooksBaseOnCategory("Horror");
                Dramat = ReturnBooksBaseOnCategory("Dramat");
                Biografia = ReturnBooksBaseOnCategory("Biografia");
                Przygodowa = ReturnBooksBaseOnCategory("Przygodowa");
                Naukowa = ReturnBooksBaseOnCategory("Naukowa");
            }
        }

        private Book _selectedbook = new Book();
        public Book SelectedBook
        {
            get
            {
                return _selectedbook;
            }
            set
            {
                _selectedbook = value;
                NotifyPropertyChanged("SelectedBook");
            }
        }

        public RelayCommand SearchBooksPageCommand { get; private set; }

        public void GoToSearchBooksPage()
        {
            
            var MyFrame = Application.Current.RootVisual as Frame;
            MyFrame.Navigate(new Uri("/View/SearchBooksView.xaml", UriKind.Relative));
        }
        private IList<Book> _horror = new List<Book>();
        public IList<Book> Horror 
        { 
            get
            {
                return _horror;
            }
            set
            {
                _horror = value;
                GroupedHorror = ReturnGroupedBooks(_horror);
                
            }
        }

        private IList<Book> _dramat = new List<Book>();
        public IList<Book> Dramat
        {
            get
            {
                return _dramat;
            }
            set
            {
                _dramat = value;
                GroupedDramat = ReturnGroupedBooks(_dramat);

            }
        }

        private IList<Book> _biografia = new List<Book>();
        public IList<Book> Biografia
        {
            get
            {
                return _biografia;
            }
            set
            {
                _biografia = value;
                GroupedBiografia = ReturnGroupedBooks(_biografia);
                
            }
        }

        private IList<Book> _przygodowa = new List<Book>();
        public IList<Book> Przygodowa
        {
            get
            {
                return _przygodowa;
            }
            set
            {
                _przygodowa = value;
                GroupedPrzygodowa = ReturnGroupedBooks(_przygodowa);
            }
        }

        private IList<Book> _naukowa = new List<Book>();
        public IList<Book> Naukowa
        {
            get
            {
                return _naukowa;
            }
            set
            {
                _naukowa = value;
                GroupedNaukowa = ReturnGroupedBooks(_naukowa);
            }
        }
        private IList<AlphaKeyGroup<Book>> _groupedhorror = new List<AlphaKeyGroup<Book>>();
        public IList<AlphaKeyGroup<Book>> GroupedHorror
        {
            get
            {
                return _groupedhorror;
            }
            set
            {
                _groupedhorror = value;
                NotifyPropertyChanged("GroupedHorror");
            }
        }

        private IList<AlphaKeyGroup<Book>> _groupeddramat = new List<AlphaKeyGroup<Book>>();
        public IList<AlphaKeyGroup<Book>> GroupedDramat
        {
            get
            {
                return _groupeddramat;
            }
            set
            {
                _groupeddramat = value;
                NotifyPropertyChanged("GroupedDramat");
            }
        }

        private IList<AlphaKeyGroup<Book>> _groupedbiografia = new List<AlphaKeyGroup<Book>>();
        public IList<AlphaKeyGroup<Book>> GroupedBiografia
        {
            get
            {
                return _groupedbiografia;
            }
            set
            {
                _groupedbiografia = value;
                NotifyPropertyChanged("GroupedBiografia");
            }
        }

        private IList<AlphaKeyGroup<Book>> _groupedprzygodowa = new List<AlphaKeyGroup<Book>>();
        public IList<AlphaKeyGroup<Book>> GroupedPrzygodowa
        {
            get
            {
                return _groupedprzygodowa;
            }
            set
            {
                _groupedprzygodowa = value;
                NotifyPropertyChanged("GroupedPrzygodowa");
            }
        }

        private IList<AlphaKeyGroup<Book>> _groupednaukowa = new List<AlphaKeyGroup<Book>>();
        public IList<AlphaKeyGroup<Book>> GroupedNaukowa
        {
            get
            {
                return _groupednaukowa;
            }
            set
            {
                _groupednaukowa = value;
                NotifyPropertyChanged("GroupedNaukowa");
            }
        }

        public IList<Book> ReturnBooksBaseOnCategory(string category)
        {
            IList<Book> list = new List<Book>();
            if (Books != null)
            {
                foreach (Book b in Books)
                {
                    foreach (Category c in b.Categories)
                    {
                        if (c.CategoryName == category)
                        {
                            list.Add(b);
                        }
                    }
                }
            }
            return list;
        }

        public IList<AlphaKeyGroup<Book>> ReturnGroupedBooks(IList<Book> books)
        {
            return AlphaKeyGroup<Book>.CreateGroups(
                    books,
                    (Book b) => { return b.Title; },
                    true);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}