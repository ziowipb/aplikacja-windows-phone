﻿using GalaSoft.MvvmLight;
using LibraryWindowsPhone.Model;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows;
using System.Windows.Controls;

namespace LibraryWindowsPhone.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class DetailsBookViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the DetailsBookViewModel class.
        /// </summary>
        public DetailsBookViewModel()
        {
            
        }

        public Book book
        {
            get
            {
                return (Book)PhoneApplicationService.Current.State["details"];
            }
        }
    }
}