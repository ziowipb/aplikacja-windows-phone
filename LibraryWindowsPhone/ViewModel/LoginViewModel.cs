﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Windows;
using System.Windows.Controls;

namespace LibraryWindowsPhone.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class LoginViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the LoginViewModel1 class.
        /// </summary>
        public LoginViewModel()
        {
            MainPageCommand = new RelayCommand(() => GoToMainPage());
        }
        
        public RelayCommand MainPageCommand { get; private set; }

        public void GoToMainPage()
        {
            var MyFrame = Application.Current.RootVisual as Frame;
            MyFrame.Navigate(new Uri("/View/MainView.xaml", UriKind.Relative));
        }
    }
}