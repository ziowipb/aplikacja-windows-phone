using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LibraryWindowsPhone.Model;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;

namespace LibraryWindowsPhone.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            BrowseBooksPageCommand = new RelayCommand(() => GoToBrowseBooksPage());
            SearchBooksPageCommand = new RelayCommand(() => GoToSearchBooksPage());
            CloseApplicationCommand = new RelayCommand(() => CloseApplication());
            
        }

        public IList<Book> books;
        public IList<Category> categories;
        public RelayCommand BrowseBooksPageCommand { get; private set; }

        public void GoToBrowseBooksPage()
        {
            
            
            var MyFrame = Application.Current.RootVisual as Frame;
            MyFrame.Navigate(new Uri("/View/BrowseBooksView.xaml", UriKind.Relative));
        }

        public RelayCommand CloseApplicationCommand { get; private set; }

        public void CloseApplication()
        {
            Application.Current.Terminate();
        }
        public RelayCommand SearchBooksPageCommand { get; private set; }

        public void GoToSearchBooksPage()
        {

            var MyFrame = Application.Current.RootVisual as Frame;
            MyFrame.Navigate(new Uri("/View/SearchBooksView.xaml", UriKind.Relative));
        }

        
        

    }
}