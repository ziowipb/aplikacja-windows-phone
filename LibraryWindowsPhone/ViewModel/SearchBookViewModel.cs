﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LibraryWindowsPhone.Model;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace LibraryWindowsPhone.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SearchBookViewModel : ViewModelBase, INotifyPropertyChanged
    {
        /// <summary>
        /// Initializes a new instance of the SearchBookViewModel class.
        /// </summary>
        public SearchBookViewModel()
        {
            Category cat = new Category { CategoryId = 0, CategoryName = "brak" };
            Categories.Add(cat);
            Categories.AddRange( (List<Category>)PhoneApplicationService.Current.State["categories"]);
            
            SearchBookCommand = new RelayCommand(() => SearchBook());
        }

        public RelayCommand SearchBookCommand { get; private set; }

        public void SearchBook()
        {
            IList<Book> books =(List<Book>)PhoneApplicationService.Current.State["books"];
            IList<Book> result = new List<Book>();
            string title = TxtboxTitle == null ? "" : TxtboxTitle;
            string author = TxtboxAuthor == null ? "" : TxtboxAuthor;
            string isbn = TxtboxIsbn == null ? "" : TxtboxIsbn;
            
            foreach(Book b in books)
            {
                if (b.Title.IndexOf(title, StringComparison.OrdinalIgnoreCase) >= 0 && b.Author.IndexOf(author, StringComparison.OrdinalIgnoreCase) >= 0 && b.ISBN.IndexOf(isbn, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    if(SelectedCategory == 0)
                    {
                        result.Add(b);
                        continue;
                    }
                    foreach(Category c in b.Categories)
                    {
                        if(SelectedCategory == c.CategoryId)
                        {
                            result.Add(b);
                            continue;
                        }
                    }
                }
                
            }

            PhoneApplicationService.Current.State["search"] = result;
            var MyFrame = Application.Current.RootVisual as Frame;
            MyFrame.Navigate(new Uri("/View/SearchResultView.xaml", UriKind.Relative));

        }

        private string _txtboxTitle;
        public string TxtboxTitle 
        { 
            get
            {
                return _txtboxTitle;
            }
            set
            {
                _txtboxTitle = value;
                NotifyPropertyChanged("TxtboxTitle");
            }
        }

        private string _txtboxAuthor;
        public string TxtboxAuthor
        {
            get
            {
                return _txtboxAuthor;
            }
            set
            {
                _txtboxAuthor = value;
                NotifyPropertyChanged("TxtboxAuthor");
            }
        }


        private string _txtboxIsbn;
        public string TxtboxIsbn
        {
            get
            {
                return _txtboxIsbn;
            }
            set
            {
                _txtboxIsbn = value;
                NotifyPropertyChanged("TxtboxIsbn");
            }
        }

        private int selectedCategory;
        public int SelectedCategory 
        {
            get
            {
                return selectedCategory;
            }
            set
            {
                selectedCategory = value;
                NotifyPropertyChanged("SelectedCategory");
            }
        }

        private List<Category> _categories = new List<Category>();
        public List<Category> Categories
        {
            get
            {
                return _categories;
            }
            set
            {
                
                _categories = value;
                NotifyPropertyChanged("Categories");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        
    }
}