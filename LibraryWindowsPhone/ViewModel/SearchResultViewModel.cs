﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LibraryWindowsPhone.Helper;
using LibraryWindowsPhone.Model;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace LibraryWindowsPhone.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SearchResultViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the SearchResultViewModel class.
        /// </summary>
        public SearchResultViewModel()
        {
                DetailsBookPageCommand = new RelayCommand<Book>(GoToDetailsBookPage);
        }

        public ICommand DetailsBookPageCommand { get; set; }

        public void GoToDetailsBookPage(Book book)
        {
            PhoneApplicationService.Current.State["details"] = book;
            var MyFrame = Application.Current.RootVisual as Frame;
            MyFrame.Navigate(new Uri("/View/DetailsBookView.xaml", UriKind.Relative));
        }

        public IList<Book> Result
        {
            get
            {
                return (List<Book>)PhoneApplicationService.Current.State["search"];
            }
        }

        public IList<AlphaKeyGroup<Book>> GroupedResult
        {
            get
            {
                return AlphaKeyGroup<Book>.CreateGroups(
                    Result,
                    (Book b) => { return b.Title; },
                    true);
            }
        }
        private Book _selectedbook;
        public Book SelectedBook
        {
            get
            {
                return _selectedbook;
            }
            set
            {
                _selectedbook = value;
                NotifyPropertyChanged("SelectedBook");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}